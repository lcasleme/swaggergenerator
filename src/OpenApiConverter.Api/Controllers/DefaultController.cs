using Microsoft.AspNetCore.Mvc;
namespace OpenApiConverter.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class DefaultController : ControllerBase
{
    public DefaultController()
    {
    }

    [HttpPost]
    [Route("[route]")]
    [ProducesResponseType(typeof(Classe), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ClasseErro), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(ClasseErro), StatusCodes.Status500InternalServerError)]
    [ProducesResponseType(typeof(ClasseErro), StatusCodes.Status404NotFound)]
    public ClasseResponse PostShipment(Classe request)
    {
        return new ClasseResponse();
    }
}
